﻿/**
* File picker class
* Gets file descriptor
* Reads file from disc
*/

(function () {
    var FilePicker = WinJS.Class.define(function() {
        this.file = null;
    }, {
        // picks file
        pickFiles: function () {
            var that = this;
            // Create the picker object and set options
            var openPicker = new Windows.Storage.Pickers.FileOpenPicker();

            openPicker.viewMode = Windows.Storage.Pickers.PickerViewMode.list;
            // todo edit directory
            openPicker.suggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.documentsLibrary;

            // filter by file extension
            openPicker.fileTypeFilter.replaceAll(['.txt']);

            openPicker.pickSingleFileAsync().then(function (file) {
                if (file) {
                    // Application now has read/write access to the picked file
                    WinJS.log && WinJS.log('File was picked: ' + file.name);
                    that.file = file;

                    Windows.Storage.FileIO.readTextAsync(file).done(function (text) {
                        WinJS.log && WinJS.log('Data was read: ' + text.split('\t').length);
                        that.data = text;
                    }, function(err) {
                        // TODO Error handling
                    });
                }
            });
        }
    });
    

    // export function
    WinJS.Namespace.define('global', {
        FilePicker: FilePicker
    });
})();