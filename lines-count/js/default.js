﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkId=232509
(function () {
    'use strict';

    var app = WinJS.Application;
    var activation = Windows.ApplicationModel.Activation;

    app.onactivated = function (args) {
        if (args.detail.kind === activation.ActivationKind.launch) {
            if (args.detail.previousExecutionState !== activation.ApplicationExecutionState.terminated) {
                // TODO: This application has been newly launched. Initialize
                // your application here.
            } else {
                // TODO: This application has been reactivated from suspension.
                // Restore application state here.
            }

            args.setPromise(WinJS.UI.processAll().then(function () {
                var btnFilePicker = $('#filePicker');
                var btnSubmit = $('#submit');
                var cbShowLines = $('#showLines');
                var cbPlot = $('#plot');
                var filePicker = null;
                var drawer = null;
                var source = null;

                btnFilePicker.on('click', function () {
                    // get file descriptor
                    filePicker = new global.FilePicker();
                    filePicker.pickFiles();
                });

                btnSubmit.on('click', function () {
                    WinJS.log && WinJS.log('Submit button clicked');

                    // params
                    var threshold = parseInt($('#threshold').val().trim());
                    var landmark = parseInt($('#angle').val().trim());
                    var interference = parseInt($('#interference').val().trim());
                    var deflection = parseInt($('#deflection').val().trim());
                    var showLines = cbShowLines.is(':checked');
                    var plot = cbPlot.is(':checked');

                    threshold = isNaN((threshold)) ? 0 : threshold;
                    landmark = landmark < 0 ? landmark + 180 : landmark;
                    interference = isNaN(interference) ? 5 : interference;
                    deflection = isNaN((deflection)) ? 10 : deflection;

                    if (filePicker && filePicker.file && filePicker.data) {
                        // analyze data
                        WinJS.log && WinJS.log('Step 1: data analyzing');
                        source = new global.Source(filePicker.data, landmark, threshold, interference, deflection);
                        source.count();

                        // plot data
                        WinJS.log && WinJS.log('Step 2: drawing');
                        drawer = new global.Drawer('#chart', source.dataset, source.lines, showLines, plot);
                        drawer.draw();
                    }
                });

                cbShowLines.change(function () {
                    WinJS.log && WinJS.log('Checkbox was changed');
                    if (drawer) {
                        drawer.showLines = cbShowLines.is(':checked');
                        drawer.plot = cbPlot.is(':checked');
                        drawer.draw();
                    }
                });

                cbPlot.change(function () {
                    WinJS.log && WinJS.log('Checkbox was changed');
                    if (drawer) {
                        drawer.showLines = cbShowLines.is(':checked');
                        drawer.plot = cbPlot.is(':checked');
                        drawer.draw();
                    }
                });
            }));
        }
    };

    app.oncheckpoint = function (args) {
        // TODO: This application is about to be suspended. Save any state
        // that needs to persist across suspensions here. You might use the
        // WinJS.Application.sessionState object, which is automatically
        // saved and restored across suspension. If you need to complete an
        // asynchronous operation before your application is suspended, call
        // args.setPromise().
    };

    app.start();
})();
