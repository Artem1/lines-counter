﻿/** 
* Source class
* Implements wave algorithm for searching shapes 
*/

(function () {
    var Source = WinJS.Class.define(function (text, landmark, threshold, interference, deflection) {
        this._landmark = landmark;
        this._threshold = threshold;
        this._interference = interference;
        this._deflection = deflection;
        this._parser = new global.Parser(text, this._threshold, this._interference);
        this._filter = new global.Filter(this._interference, this._landmark, this._deflection);
        this._data = this._parser.getData();
        this.dataset = this._parser.getDataset(); // scatter plot data
        this._bookmark = { x: 0, y: 0 };
        this._shapes = []; // massive of found lines
        this.lines = []; // massive of filtered lines
    }, {
        // finds unaccounted point
        _next: function () {
            for (var y = this._bookmark.y; y < this._data.length; y++, this._bookmark.x = 0) {
                for (var x = this._bookmark.x; x < this._data[y].length; x++) {
                    var point = {
                        x: x,
                        y: y
                    };
                    
                    // check if point has been counted already
                    if (this._validate(point, this._shapes)) {
                        // new shape found
                        this._bookmark = point;

                        return true;
                    }
                }
            }
            
            // not found
            return false;
        },

        // validates point before push
        _validate: function (p, r) {
            var point = null;

            if (this._data[p.y]      === undefined ||
                this._data[p.y][p.x] === 0         ||
                this._data[p.y][p.x] === undefined) {
                return false;
            }

            for (var i = 0; i < r.length; i++) {
                if (r[i] instanceof Array) {
                    for (var j = 0; j < r[i].length; j++) {
                        point = r[i][j];

                        if (point.x === p.x && point.y === p.y) {
                            return false;
                        }
                    }
                } else {
                    point = r[i];

                    if (point.x === p.x && point.y === p.y) {
                        return false;
                    }
                }
            }

            return true;
        },

        // continues wave one step further
        _continue: function(p, result) {
            var wave = [];
            var candidates = [
                { x: p.x - 1, y: p.y + 1 },
                { x: p.x,     y: p.y + 1 },
                { x: p.x + 1, y: p.y + 1 },
                { x: p.x + 1, y: p.y     },
                { x: p.x - 1, y: p.y     }
            ];

            for (var i = 0; i < candidates.length; i++) {
                if (this._validate(candidates[i], result)) {
                    wave.push(candidates[i]);
                }
            }

            return wave;
        },

        // analyzes wave
        _analyze: function(wave) {
            wave = wave.sort(function (a, b) { return a.x > b.x; });

            var min = d3.min(wave, function(p) { return p.x; });
            var max = d3.max(wave, function (p) { return p.x; });

            for (var x = min; x <= max; x++) {
                var exist = false;

                for (var i = 0; i < wave.length; i++) {
                    if (wave[i].x === x) {
                        exist = true;
                        break;
                    }
                }

                if (!exist) {
                    return true;
                }
            }

            return false;
        },

        // generates wave
        _wave: function (shape, generation) {
            var point = null;
            var wave = [];

            shape = shape.concat(generation);

            // continue spreading 
            for (var i = 0; i < generation.length; i++) {
                point = generation[i];
                wave = wave.concat(this._continue(point, shape.concat(wave)));
            }

            var separation = this._analyze(wave);

            if (wave.length > 0 && !separation) {
                // new generation
                generation = wave;
                // continue searching
                shape = this._wave(shape, generation);
            }

            return shape;
        },

        // finds lines
        _findLines: function() {
            while (this._next()) {
                var shape = [];
                var generation = [];

                // push start point
                generation.push({ x: this._bookmark.x, y: this._bookmark.y });

                shape = this._wave(shape, generation);

                // sort points
                function comparator(a, b) {
                    var deltaX = a.x - b.x;
                    var deltaY = a.y - b.y;

                    return deltaY ? deltaY : deltaX;
                }

                this._shapes.push(shape.sort(comparator));
            }

            WinJS.log && WinJS.log('Step 1.1.2: common filtering');
            this._shapes = this._filter.filter(this._shapes);
            WinJS.log && WinJS.log('Step 1.1.2 completed. Lines: ' + this._shapes.length);

            WinJS.log && WinJS.log('Step 1.1.3: angle filtering');
            this.lines = this._filter.filterByAngle(this._shapes);
            WinJS.log && WinJS.log('Step 1.1.3 completed. Lines found: ' + this.lines);

            this.dataset = this._parser.parse(this._shapes);
        },

        // gets result
        count: function () {
            WinJS.log && WinJS.log('Step 1.1: wave spreading');
            this._findLines();
            WinJS.log && WinJS.log('Step 1.1 completed. Lines: ' + this._shapes.length);

            return this.lines;
        }
    });

    // export function
    WinJS.Namespace.define('global', {
        Source: Source
    });
})();