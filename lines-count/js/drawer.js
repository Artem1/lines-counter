﻿/** 
* Drawer class
* Binds data to DOM using D3 Data-Driven Documents library
* Draws chart
*/

(function () {
    var Drawer = WinJS.Class.define(function (selector, dataset, lines, showLines, plot) {
        this._selector = selector;
        this._dataset = dataset;
        this._lines = lines;
        this.showLines = showLines;
        this.plot = plot;
    }, {
        _tabulate: function () {
            var table = d3.select(this._selector)
                .append('table')
                .attr('class', 'table');
            var thead = table.append('thead');
            var tbody = table.append('tbody');
            var columns = ['#', 'p1', 'p2', '∠'];

            // append the header row
            thead.append('tr')
                .selectAll('th')
                .data(columns)
                .enter()
                .append('th')
                .text(function (column) { return column; });

            // create a row for each object in the data
            var rows = tbody.selectAll('tr')
                .data(this._lines)
                .enter()
                .append('tr');

            // create a cell in each row for each column
            var cells = rows.selectAll('td')
                .data(function (line, i) {
                    return columns.map(function (column, j) {
                        var value = [
                            i + 1,
                            '[' + line[0].x + ', ' + line[0].y + ']',
                            '[' + line[1].x + ', ' + line[1].y + ']',
                            line[2]
                        ];

                        return {
                            column: column,
                            value: value[j]
                        };
                    });
                })
                .enter()
                .append('td')
                .html(function (d) {
                    return d.value;
                });
        },

        // draws data and lines
        draw: function () {
            WinJS.log && WinJS.log('Step 2.1: drawing axes');
            // clear chart first
            $(this._selector).empty();

            var margin = { top: 20, right: 15, bottom: 60, left: 60 };
            var xMax = d3.max(this._dataset, function (d) { return d[0]; });
            var yMax = d3.max(this._dataset, function (d) { return d[1]; });
            var width = xMax * 3 - margin.left - margin.right;
            var height = yMax * 3 - margin.top - margin.bottom;
            var x = d3.scale.linear()
                .domain([0, xMax])
                .range([0, width]);
            var y = d3.scale.linear()
                .domain([0, yMax])
                .range([height, 0]);
            var chart = d3.select(this._selector)
                .append('svg:svg')
                .attr('width', width + margin.right + margin.left)
                .attr('height', height + margin.top + margin.bottom)
                .attr('class', 'chart');
            var main = chart.append('g')
                .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
                .attr('width', width)
                .attr('height', height)
                .attr('class', 'main axis');

            // draw the x axis
            var xAxis = d3.svg.axis()
                .scale(x)
                .orient('bottom');
            // draw the y axis
            var yAxis = d3.svg.axis()
                .scale(y)
                .orient('left');

            main.append('g')
                .attr('transform', 'translate(0,' + height + ')')
                .attr('class', 'main')
                .call(xAxis);
            main.append('g')
                .attr('transform', 'translate(0,0)')
                .attr('class', 'main axis')
                .call(yAxis);

            var g = main.append('svg:g');

            WinJS.log && WinJS.log('Step 2.1 completed. Width: ' + width + '. Heigth: ' + height);

            // show scatter plot
            if (this.plot) {
                WinJS.log && WinJS.log('Step 2.2.1: drawing scatter plot');

                g.selectAll('scatter-dots')
                    .data(this._dataset)
                    .enter().append('svg:circle')
                    .attr('cx', function(d, i) { return x(d[0]); })
                    .attr('cy', function(d) { return y(d[1]); })
                    .attr('r', 2);

                WinJS.log && WinJS.log('Step 2.2.1 completed');
            }

            // show lines
            if (this.showLines) {
                WinJS.log && WinJS.log('Step 2.2.2: drawing lines');

                for (var i = 0; i < this._lines.length; i++) {
                    var p1 = this._lines[i][0];
                    var p2 = this._lines[i][1];

                    main.append('line')
                        .attr('x1', x(p1.x))
                        .attr('x2', x(p2.x))
                        .attr('y1', y(p1.y))
                        .attr('y2', y(p2.y))
                        .attr('stroke', 'darkblue')
                        .attr('stroke-width', '1px');
                }

                WinJS.log && WinJS.log('Step 2.2.2 completed');
            }

            if (this._lines.length > 0) {
                this._tabulate();
            }

            // display chart
            $(this._selector).show();
        }
    });

    
    
    // export function
    WinJS.Namespace.define('global', {
        Drawer: Drawer
    });
})();