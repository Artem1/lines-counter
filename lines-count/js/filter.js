﻿/** 
* Filter class
* Filters data by size, angle
*/

(function () {
    var Filter = WinJS.Class.define(function (interference, landmark, deflection) {
        this._interference = interference;
        this._landmark = landmark;
        this._deflection = deflection;
    }, {
        // calculates angle
        _findAngle: function (p1, p2) {
            var radians = Math.atan2(p2.y - p1.y, p2.x - p1.x);
            var angle = radians * 180 / Math.PI;

            return angle;
        },

        // filters data
        filter: function (shapes) {
            var that = this;

            // filter by size
            return shapes.filter(function (shape, i, result) {
                return shape.length >= that._interference;
            });
        },

        // filters lines by angle
        filterByAngle: function (shapes) {
            var that = this;
            var lines = [];

            lines = shapes.filter(function (shape, i, result) {
                var angle = that._findAngle(shape[0], shape[shape.length - 1]);

                return angle >= that._landmark - that._deflection && angle <= that._landmark + that._deflection;
            });

            // remove excessive data
            lines = lines.map(function (shape, i, result) {
                return [
                    shape[0],
                    shape[shape.length - 1],
                    that._findAngle(shape[0], shape[shape.length - 1]).toFixed(2)];
            });

            return lines;
        }

        // TODO horizontal & vertical filters
    });

    // export function
    WinJS.Namespace.define('global', {
        Filter: Filter
    });
})();