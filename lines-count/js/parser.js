﻿/** 
* Parser class
* Parses data from string to dataset
*/

(function() {
    var Parser = WinJS.Class.define(function (text, threshold, interference) {
        // converts db level to custom value level
        this._convertDbtoVal = function(db) {
            if (!db && db !== 0) {
                return 0;
            }

            return Math.round(64 * Math.pow(10, db / 20));
        };
        this._text = text;
        this._threshold = this._convertDbtoVal(threshold);
        this._interference = interference;
        this._data = null;
        this._dataset = null;
    }, {
        // parses data
        getData: function () {
            var that = this;

            // read data, reverse it & filter
            this._data = d3.tsv.parseRows(this._text).reverse().map(function (y, lines) {
                return y.map(function (x, line) {
                    if (x > that._threshold) {
                        return x;
                    } else {
                        return 0;
                    }
                });
            });

            return this._data;
        },

        // converts data to dataset
        getDataset: function () {
            if (!this._data) {
                this.getData();
            }

            // convert array of values to dataset
            this._dataset = this._data.reduce(function (accumulator, line, y, lines) {
                line.map(function (value, x, line) {
                    // FIXME
                    if (value > 0) {
                        accumulator.push([x, y]);
                    }
                });

                return accumulator;
            }, []);

            return this._dataset;
        },

        // parses massive of shapes to dataset
        parse: function(shapes) {
            return shapes.reduce(function(accumulator, shape, i, shapes) {
                shape.map(function(p, j, shape) {
                    accumulator.push([p.x, p.y]);
                });

                return accumulator;
            }, []);
        }
    });

    // export function
    WinJS.Namespace.define('global', {
        Parser: Parser
    });
})();